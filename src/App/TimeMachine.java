package App;


import java.util.concurrent.TimeUnit;

/**
 * Used as a timer
 * passes information to start a new day
 */
public class TimeMachine extends Thread {
    private Panel panel;
    public TimeMachine(Panel panel){
        this.panel = panel;
    }
    public void run(){
        while(true){
            try {
                TimeUnit.SECONDS.sleep(10);
                panel.nextDay();
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }

        }
    }

}
