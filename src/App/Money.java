package App;

import App.CurrencyMarket.Currency;

/**
 * class used to represent Investor's money - currency and amount
 */
public class Money extends Asset {
    private Currency currency;


    /**
     * creates a "wallet" of given currency
     * sets amount to 0
     * @param currency - Currency() reference
     *
     */
    public Money(Currency currency) {
        this.setName(currency.getName());
        this.currency = currency;
        setAmount(0);
    }
    /**
     * creates a "wallet" of given currency and sets the amount
     * @param currency - Currency() reference
     * @param amount - amount of money in given currency
     */
    public Money(Currency currency, float amount){
        this.setName(currency.getName());
        this.currency = currency;
        setAmount(amount);
    }

    /**
     * creates a "wallet" with given currencyName (String instead of Currency())
     * @param currencyName
     * @param num
     */
    public Money(String currencyName, float num){
        this.setName(currencyName);
        for (int i=0;i<Currency.currenciesAlive.size();i++){
            if (Currency.currenciesAlive.get(i).getName().equals(currencyName)){
                this.currency = Currency.currenciesAlive.get(i);
                setAmount(num);
            }
        }
    }

    /**
     * adds given amount of money
     * @param num
     */
    public void add(float num){
        this.setAmount(this.getAmount() + num);
    }

    /**
     * subtracts given amount of money
     * @param num
     */
    public void subtract(float num){
        this.setAmount(this.getAmount() - num);
    }

    /**
     * @return reference to Currency object
     */
    public Currency getCurrency() {
        return currency;
    }
    public String toString(){
        return currency + ": " + getAmount();
    }
}
