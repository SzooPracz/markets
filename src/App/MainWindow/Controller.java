package App.MainWindow;

import App.*;
import App.Company.Company;
import App.CurrencyMarket.Currency;
import App.Index.Index;
import App.Investor.Investor;
import App.RawMaterialsMarket.RawMaterial;
import App.RawMaterialsMarket.RawMaterialEntry;
import App.StockExchange.StockExchange;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;


public class Controller {

    @FXML private HBox hbox;
    @FXML private ListView<StockExchange> marketList;
    @FXML private ListView<Company> companyList;
    @FXML private ListView<Investor> investorList;
    @FXML private ListView<Currency> currencyList;
    @FXML private ListView<Index> indexList;
    @FXML private ListView<RawMaterialEntry> rawMaterialList;
    @FXML private TitledPane rawMaterialsPane;
    @FXML private TitledPane indicesPane;
    private Panel model;



    public void initModel(Panel model) {

        if (this.model != null) {
            throw new IllegalStateException("Model can only be initialized once");
        }

        this.model = model;

        ObservableList<StockExchange> marketObservable = this.model.getMarketList();
        ObservableList<Company> companyObservable = this.model.getCompanies();
        ObservableList<Investor> investorObservable = this.model.getInvestors();
        ObservableList<Currency> currencyObservable = this.model.getCurrencies();
        ObservableList<RawMaterialEntry> rawMaterialObservable = this.model.getRawMaterials();

        marketList.setItems(marketObservable);
        companyList.setItems(companyObservable);
        investorList.setItems(investorObservable);
        currencyList.setItems(currencyObservable);
        rawMaterialList.setItems(rawMaterialObservable);
        rawMaterialsPane.setText("Raw Materials [" + currencyObservable.get(0).getName() +"]");
        companyList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount()==2) {
                    Company selectedCompany = companyList.getSelectionModel().getSelectedItem();
                    System.out.println("clicked on: " + selectedCompany);
                    if (selectedCompany != null) {
                        selectedCompany.startWindow();
                    }
                }
            }
        });
        marketList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                StockExchange selectedMarket = marketList.getSelectionModel().getSelectedItem();
                if (selectedMarket != null) {
                    if (selectedMarket.getIndices() != null) {
                        indexList.setItems(selectedMarket.getIndices());
                        indicesPane.setText("Indices of " + selectedMarket.getName());
                    }
                    if (event.getClickCount()==2){
                        selectedMarket.startWindow();
                    }
                }

            }
        });
        investorList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount()==2) {
                    Investor selectedInvestor = investorList.getSelectionModel().getSelectedItem();
                    if (selectedInvestor != null) {
                        selectedInvestor.startWindow();
                    }
                }
            }
        });
        currencyList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount()==2) {
                    Currency selectedCurrency = currencyList.getSelectionModel().getSelectedItem();
                    if (selectedCurrency != null) {
                        model.getCurrencyMarket().startWindow(selectedCurrency);
                    }
                }
            }
        });
        indexList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount()==2){
                    Index selectedIndex = indexList.getSelectionModel().getSelectedItem();
                    if (selectedIndex != null){
                        selectedIndex.startWindow();
                    }
                }
            }
        });
        rawMaterialList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2){
                    RawMaterialEntry selectedMaterial = rawMaterialList.getSelectionModel().getSelectedItem();
                    if (selectedMaterial != null){
                        model.getRawMaterialsMarket().startWindow(selectedMaterial);
                    }
                }
            }
        });
    }
    public void refresh(){
        marketList.refresh();
        companyList.refresh();
        currencyList.refresh();
        investorList.refresh();
        indexList.refresh();
        rawMaterialList.setItems(this.model.getRawMaterials());
        rawMaterialList.refresh();
    }
    public void addMarket(ActionEvent actionEvent) {
        this.model.addMarket();
    }
    public void addCompany(ActionEvent actionEvent){
        this.model.addCompany();
    }
    public void addInvestor(ActionEvent actionEvent){
        this.model.addInvestor();
    }
    public void addCurrency(ActionEvent actionEvent){
        this.model.addCurrency();
    }
    public void addRawMaterial(ActionEvent actionEvent) {this.model.addRawMaterial();}
    public void addIndex(ActionEvent actionEvent) {
        StockExchange market = this.marketList.getSelectionModel().getSelectedItem();
        if (market != null){
            market.addIndex();
        }
    }
    public void deleteCompany(){
        this.model.deleteCompany(companyList.getSelectionModel().getSelectedItem());
    }

    public void deleteStockExchange() {
        this.model.deleteStockExchange(marketList.getSelectionModel().getSelectedItem());
    }


    public void lock() {
        hbox.setDisable(true);
    }

    public void unlock() {
        refresh();
        hbox.setDisable(false);
    }
}
