package App.MainWindow;

import App.Panel;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class MainWindow extends Application {
    public static Stage rootStage;
    private final Panel panel;
    public static Controller controller;
    public MainWindow() {
        this.panel = new Panel();
    }


    @Override
    public void start(Stage stage) throws Exception{
        if (rootStage == null){
            rootStage = stage;
        }
        BorderPane root = new BorderPane();
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("view.fxml"));
        root.setCenter(viewLoader.load());
        controller = viewLoader.getController();
        controller.initModel(panel);

        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                System.out.println("Stage is closing");
                panel.destroy();
            }
        });


    }


    public static void main(String[] args) {
        launch(args);
    }
}
