package App.Company;

import App.StockExchange.StockExchange;

import java.util.*;


public class Company {
    private static final List<String> allNames;
    static{
        allNames = new ArrayList<>(
                Arrays.asList("Biełaruskaja czyhunka","Byłgarski Dyrżawni Żełeznici","České dráhy", "Danske Statsbaner" , "Nordjyske Jernbaner",
                        "Eurotunel", "Euro Cargo Rail", "EuskoTren", "Arriva", "Deutsche Bahn", "DB Regio", "S-Bahn Berlin",
                        "Norges StatsBaner", "PKP Intercity", "Koleje Śląskie", "Infra Silesia", "British Rail", "Heathrow Express", "Amtrak",
                        "Japan Railway")
        );
    }
    private StockExchange stock;
    private String name;
    private int numOfShares;
    private float price;
    private int daySetup;



    public Company() {
        this.issueShares();
        this.setPrice(10);

        int size = allNames.size();
        if (size == 0) {
            this.setName("Noname");
        } else {
            int picked = new Random().nextInt(size);
            this.setName(allNames.get(picked));
            allNames.remove(picked);
        }
        System.out.println("new company added: " + name);
    }
    public void startWindow(){
        CompanyWindow window = new CompanyWindow(this);
    }
    @Override
    public String toString() {
        return this.name;
    }
    public String getName(){return this.name;}
    public void setName(String name) {
        this.name = name;
    }

    public void issueShares(){
        this.numOfShares += 10;
    }


    public int getNumOfShares() {
        return numOfShares;
    }

    public float getPrice() {
        return price;
    }

    public void setStock(StockExchange stock) {
        this.stock = stock;
    }

    public StockExchange getStock() {
        return stock;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getDaySetup() {
        return daySetup;
    }

    public void setDaySetup(int daySetup) {
        this.daySetup = daySetup;
    }

    public void update(int day) {
        float change =new Random().nextInt(101) - 50;
        change = change / 1000;
        setPrice(price + price*change);
    }
}
