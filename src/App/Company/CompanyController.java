package App.Company;

import App.MainWindow.MainWindow;
import App.Validator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class CompanyController {
    @FXML private TextField nameField;
    @FXML private AnchorPane pane;
    @FXML private Label numOfSharesLabel;
    @FXML private Label marketNameLabel;
    private Company model;

    public CompanyController(){

    }

    public void nameConfirm(ActionEvent actionEvent){
        String newName = nameField.getText();
        if (Validator.Name(newName)){
            this.model.setName(newName);
            System.out.println("new name set: " + newName);
            MainWindow.controller.refresh();
            this.closeWindow();
        } else {
            System.out.println("new name does not fit");
        }

    }
    public void deleteCompany(){
        MainWindow.controller.deleteCompany();
        this.closeWindow();
    }

    public void initModel(Company company) {
        model = company;

        nameField.setText(model.getName());
        numOfSharesLabel.setText(String.valueOf(model.getNumOfShares()));
        marketNameLabel.setText(model.getStock().getName());
    }
    public void issueShares(){
        this.model.issueShares();
        this.numOfSharesLabel.setText(String.valueOf(model.getNumOfShares()));
    }
    private void closeWindow(){
        Stage stage = (Stage) this.pane.getScene().getWindow();
        stage.close();
    }



}
