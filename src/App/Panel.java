package App;

import App.Company.Company;
import App.CurrencyMarket.Currency;
import App.CurrencyMarket.CurrencyMarket;
import App.Index.Index;
import App.Investor.Investor;
import App.MainWindow.MainWindow;
import App.RawMaterialsMarket.RawMaterialEntry;
import App.RawMaterialsMarket.RawMaterialsMarket;
import App.StockExchange.StockExchange;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class for all-the-world management
 * keep references to all objects like: companies, markets, investors, currencies, threads
 */
public class Panel {
    private ObservableList<StockExchange> markets;
    private ObservableList<Company> companies;
    private ObservableList<Investor> investors;
    private ObservableList<Currency> currencies;
    private final CurrencyMarket currencyMarket;
    private int day;
    private final TimeMachine timeMachine;
    private RawMaterialsMarket rawMaterialsMarket;
    public Panel() {
        day =0;
        this.timeMachine = new TimeMachine(this);
        this.timeMachine.start();
        this.companies = FXCollections.observableArrayList();
        ObservableList<Index> indices = FXCollections.observableArrayList();
        this.markets = FXCollections.observableArrayList();
        this.investors = FXCollections.observableArrayList();
        this.currencies = FXCollections.observableArrayList();
        List<Thread> threads = new ArrayList<>();

        currencyMarket = new CurrencyMarket("Kantorek");
        currencyMarket.setMargin((float) 0.02);


        this.addCurrency();
        this.addMarket();
        this.addCompany();
        this.addInvestor();
        rawMaterialsMarket = new RawMaterialsMarket("Kopalinki");
        rawMaterialsMarket.setMargin((float) 0.03);
        this.addRawMaterial();






    }

    public ObservableList<StockExchange> getMarketList(){
        return markets;
    }
    public ObservableList<Company> getCompanies() { return companies; }
    public ObservableList<Investor> getInvestors() { return investors; }
    public ObservableList<Currency> getCurrencies(){return currencies;}

    /**
     * adds a new automatically generated {Company} which can be edited later
     * connects the company with a random {StockExchange}
     */
    public void addCompany(){ // adding a new company to list and to random StockExchange
        Company newOne = new Company();
        int size = this.markets.size();
        if (size > 0) {
            int picked = new Random().nextInt(size);
            StockExchange market = this.markets.get(picked);
            market.addToIndex(newOne);
            newOne.setStock(market);

        }

        this.companies.add(newOne);
    }

    /**
     * adds a new automatically generated {StockExchange}
     */
    public void addMarket(){ this.markets.add(new StockExchange()); }
    public void addInvestor(){
        Investor brandNewInvestor = new Investor(this);
        Thread brandNewThread = new Thread(brandNewInvestor);
        this.investors.add(brandNewInvestor);
        try {
            brandNewThread.start();
        }catch(java.lang.IllegalStateException ex){
            System.out.println("javafx doesn't like your threads interfering with the ui!");
            ex.printStackTrace();
        }catch (Exception ex){
            System.out.println("exception when running thread");
            ex.printStackTrace();
        }

    }

    /**
     * adds a new {Currency} with random unique name
     * adds the {Currency} to {currencyMarket}
     */
    public void addCurrency(){
        Currency newOne = new Currency();
        this.currencies.add(newOne);
        this.currencyMarket.addCurrency(newOne);
    }


    /**
     * kills threads (notifies to end) when the window is closed
     */
    public void destroy() {
        for (Investor investor : investors) {
            investor.setFlag(0);

        }
        timeMachine.stop();
    }

    /**
     * Deletes a company (chosen by user) from {companies}
     * @param company - the company to be deleted (reference)
     */
    public void deleteCompany(Company company) {
        companies.remove(company);
    }

    /**
     * Locks the interface to save the data of recent day and start a new day
     * Used to generate prices changes and save prices history
     */
    public void nextDay(){
        System.out.println("It's the end of the day, wait a sec.");

        MainWindow.controller.lock();
        for (Investor investor : investors){
            investor.lock();
        }
        this.day += 1;
        for (Company company : companies) {
            company.update(day);
        }

        currencyMarket.update(day);
        rawMaterialsMarket.update(day);


        MainWindow.controller.unlock();
        for (Investor investor : investors){
            investor.unlock();
        }
        System.out.println("new day started!");

    }

    /**
     * Deletes a {StockExchange} (chosen by user) from {markets}
     * @param stock - the StockExchange to deleted (reference)
     */
    public void deleteStockExchange(StockExchange stock) { markets.remove(stock);
    }


    /**
     * @return the only currencyMarket being held by the {Panel}
     */
    public CurrencyMarket getCurrencyMarket() {
        return currencyMarket;
    }


    /**
     * Adds a new randomly chosen {RawMaterial} with unique name
     */
    public void addRawMaterial(){
        this.rawMaterialsMarket.addRawMaterial();
    }

    /**
     * @return {ObservableList} of all Raw Materials
     */
    public ObservableList<RawMaterialEntry> getRawMaterials(){
        return this.rawMaterialsMarket.getEntries();
    }


    /**
     * @return the only RawMaterialsMarket being held by the {Panel}
     */
    public RawMaterialsMarket getRawMaterialsMarket() {
        return rawMaterialsMarket;
    }
}
