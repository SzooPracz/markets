package App.StockExchange;

import App.MainWindow.MainWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class StockExchangeWindow {

    public StockExchangeWindow(StockExchange stockExchange){

        Stage stage = new Stage();
        BorderPane root = new BorderPane();
        stage.initOwner(MainWindow.rootStage);
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("view.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        StockController controller = viewLoader.getController();
        controller.initModel(stockExchange);

        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }

}
