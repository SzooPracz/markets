package App.StockExchange;

import App.CurrencyMarket.Currency;
import App.MainWindow.MainWindow;
import App.Validator;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class StockController {
    @FXML AnchorPane pane;
    @FXML TextField nameField;
    @FXML Button nameConfirmButton;
    @FXML Label countryLabel;
    @FXML Label cityLabel;
    @FXML Label addressLabel;
    @FXML Label currencyLabel;
    @FXML Label marginLabel;

    private StockExchange model;
    public StockController(){

    }

    public void initModel(StockExchange stock){
        model = stock;

        nameField.setText(model.getName());
        countryLabel.setText(model.getCountry());
        cityLabel.setText(model.getCity());
        addressLabel.setText(model.getAddress());
        marginLabel.setText(String.valueOf(model.getMargin()));
        Currency currency = model.getCurrency();
        if (currency != null){
            currencyLabel.setText(currency.getName());
        }
    }

    public void nameConfirm(javafx.event.ActionEvent actionEvent){
        String newName = nameField.getText();
        if ((Validator.Name(newName))){
            this.model.setName(newName);
            MainWindow.controller.refresh();
            this.closeWindow();
        }
    }
    public void deleteStockExchange(){
        MainWindow.controller.deleteStockExchange();
        this.closeWindow();
    }
    private void closeWindow(){
        Stage stage = (Stage) this.pane.getScene().getWindow();
        stage.close();
    }
}
