package App.StockExchange;

import App.Company.Company;
import App.CurrencyMarket.Currency;
import App.Index.Index;
import App.Market;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.*;


public class StockExchange extends Market {
    private static final List<String> allNames;

    static {
        allNames = new ArrayList<>(
                Arrays.asList("Tokyo SE", "Shanghai SE", "Hong Kong Exchange", "Nasdaq Japan",
                        "Fukoka SE", "Q-Board", "Korea Exchange", "KOSDAQ", "Taipei Exchange", "Taiwan SE",
                        "JASDAQ", " Nagoya SE")
        );
    }


    private String country;
    private String city;
    private String address;



    private ObservableList<Index> indices;
    private float margin;


    public StockExchange(String name) {
        super(name);
    }

    public StockExchange() {
        this.indices = FXCollections.observableArrayList();
        this.indices.add(new Index("first index"));
        int size = allNames.size();
        if (size == 0) {
            this.setName("Noname");
        } else {
            int picked = new Random().nextInt(size);
            this.setName(allNames.get(picked));
            allNames.remove(picked);
        }
        country = "Poland";
        city = "San Francisco";
        address = "Unknown";
        margin = (float) 0.2;

        this.setCurrency();
        System.out.println("new stock exchange created: " + this.getName());
    }

    public void startWindow() {
        StockExchangeWindow window = new StockExchangeWindow(this);
    }

    public void addToIndex(Company co) {
        int size = this.indices.size();
        int picked = new Random().nextInt(size);
        this.indices.get(picked).addCompany(co);
    }

    public void setCurrency() { //setting random currency
        int size = Currency.currenciesAlive.size();
        if (size > 0) {
            int picked = new Random().nextInt(Currency.currenciesAlive.size());
            this.setCurrency(Currency.currenciesAlive.get(picked));
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getMargin() {
        return margin;
    }

    public void setMargin(float margin) {
        this.margin = margin;
    }

    public ObservableList<Index> getIndices() {
        return indices;
    }

    public void addIndex(){
        this.indices.add(new Index());
    }


}
