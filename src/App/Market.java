package App;

import App.CurrencyMarket.Currency;

public class Market {
    private String name;
    private float margin;
    private Currency currency;
    public Market(String name) {
        this.name = name;
        margin = 0;
    }

    public Market() {
        margin = 0;
    }

    public String toString(){
        return this.name;
    }
    public void setName(String newName) {this.name = newName; }
    public String getName() {
        return name;
    }


    public float getMargin() {
        return margin;
    }

    public void setMargin(float margin) {
        this.margin = margin;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public void setCurrency(){
        this.currency = Currency.currenciesAlive.get(0);
    }
}
