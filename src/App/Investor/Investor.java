package App.Investor;

import App.CurrencyMarket.Currency;
import App.Money;
import App.Panel;
import App.RawMaterialsMarket.RawMaterial;
import App.RawMaterialsMarket.RawMaterialsMarket;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Used as a thread
 * Class representing an investor selling and buing things in markets
 */
public class Investor implements Runnable {
    private static final List<String> allFirstNames;
    private static final List<String> allLastNames;
    static {
        allFirstNames = new ArrayList<>(
                Arrays.asList( "Jace","John", "James", "Jaden", "Jai" , "Jake", "Jan", "Jarod", "Jarvis", "Javid", "Jaxon", "Jayden", "Jaylon",
                        "Jed", "Jeevan", "Jerry", "Jessie", "Joel", "Johnaton", "Jeffrey", "Juan", "Julius", "Jeff", "Jimmie", "Jonty", "Judd", "Julio")
        );
        allLastNames = new ArrayList<>(
                Arrays.asList("Kane", "Burrow", "Cage" , "Boombon" , "Smith", "Rabczewska", "Johnson", "Davis", "Moore", "Lopez", "Bell", "Perry",
                        "Jenkins", "Ross", "Bennet", "Price", "Coleman", "Patterson", "Cooper", "Richardson")
        );
    }
    private String pesel;
    private ObservableList<Money> money;
    private ObservableList<RawMaterial> rawMaterials;
    private volatile int flag = 1;
    private String firstName;
    private String lastName;
    private Panel panel;
    private boolean locked;

    /**
     * Investor is set to have money in one currency
     * name and pesel is chosen randomly
     * @param panel - reference to main panel
     */
    public Investor(Panel panel) {
        this.money = FXCollections.observableArrayList();
        this.rawMaterials = FXCollections.observableArrayList();
        this.panel = panel;
        this.setPesel();
        // getting a name
        int size = allFirstNames.size();
        int picked = new Random().nextInt(size);
        this.setFirstName(allFirstNames.get(picked));
        size = allLastNames.size();
        picked = new Random().nextInt(size);
        this.setLastName(allLastNames.get(picked));

        System.out.println("new investor added: " + firstName);

        //getting some money
        size = Currency.currenciesAlive.size();
        if (size == 0) {
            System.out.println("oops, no currency alive, you get no money");
        } else {
            picked = new Random().nextInt(size);
            money.add(new Money(Currency.currenciesAlive.get(picked)));
            money.get(0).add(2000);
            System.out.println(money.get(0));
        }

    }

    /**
     * pesel generator
     * sets random 11-digit number
     */
    private void setPesel() {
        int firstHalf = new Random().nextInt(900000)+100000;
        int secondHalf = new Random().nextInt(90000)+10000;
        pesel = String.valueOf(firstHalf) + String.valueOf(secondHalf);
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public ObservableList<Money> getMoney() {
        return this.money;
    }


    public String toString() {
        return getName();
    }

    private void setFirstName(String name) {
        this.firstName = name;
    }
    private void setLastName(String name) {this.lastName = name;}
    public void setFlag(int val) {
        flag = val;
    }

    /**
     * Investor makes random operations with random time of brakes between the operations
     */
    @Override
    public void run() {
        while (true) {
            if (flag == 0) {
                break;
            }
            try {
                int roll = new Random().nextInt(2);
                if (roll ==1) {
                    doActions();
                }
                roll = new Random().nextInt(8) + 15;
                TimeUnit.SECONDS.sleep(roll);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Investor " + firstName + " is running.");
        }
    }

    public void startWindow() {
        InvestorWindow window = new InvestorWindow(this);
    }


    synchronized private void doActions() {
        while (locked){
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        begForMoney();
        int roll = new Random().nextInt(3);
        switch (roll) {
            case 0:
                buySomeMoney();
                break;
            case 1:
                buySomeMaterials();
                break;
            case 2:
                sellSomeMaterials();
                break;
        }

    }


    private void buySomeMaterials() {
        Money pinMoney = this.money.get(0);
        if (pinMoney.getAmount() > 0){
            float toSpend = (float) new Random().nextInt(101) + 1;
            toSpend = toSpend / 100;
            toSpend = toSpend * pinMoney.getAmount();

            String materialName = null;
            for (String name : RawMaterialsMarket.rawMaterialsAlive) {
                if (!(this.hasMaterial(name))){
                    materialName = name;
                }

            }
            if (materialName != null) {
                this.panel.getRawMaterialsMarket().buyTransaction(this,materialName,toSpend );
            }

        }
    }
    private void sellSomeMaterials() {
        int size =  this.rawMaterials.size();
        if (size==0) return;

        int picked = new Random().nextInt(size);
        RawMaterial material = this.rawMaterials.get(picked);
        float percent = new Random().nextInt(101);
        percent = percent / 100;
        float amount = material.getAmount() * percent;
        if (amount > 0) {
            this.panel.getRawMaterialsMarket().sellTransaction(this, material, amount);
        }

    }


    private boolean hasMaterial(String name) {
        if (this.getRawMaterials().size()==0) return false;
        for (RawMaterial material : this.rawMaterials) {
            if ((material.getName().equals(name)) && (material.getAmount()>0) ){
                return true;
            }

        }
        return false;

    }

    private void buySomeMoney() {
        String wantedCurrency = null;
        for (int i = 0 ;i<Currency.currenciesAlive.size();i++){
            if (!(this.hasMoney(Currency.currenciesAlive.get(i).getName()))){
                wantedCurrency = Currency.currenciesAlive.get(i).getName();
                break;
            }
        }
        if (wantedCurrency != null) {
            buyCurrency(wantedCurrency,this.money.get(0).getName(),this.money.get(0).getAmount()/4);
        }
    }

    private boolean hasMoney(String name) {
        for (Money aMoney : money) {
            try {
                if ((aMoney.getName().equals(name)) && (aMoney.getAmount() > (float)0)) {
                    return true;
                }
            } catch (NullPointerException ex){
                return false;
            }
        }
        return false;
    }

    public synchronized void receiveMoney(String currencyName, float amount){
        for (Money aMoney : money) {
            try {
                String iName = aMoney.getName();
                if (iName.equals(currencyName)) {
                    aMoney.add(amount);
                    return;
                }
            } catch (NullPointerException ex) {
                break;
            }
        }

        this.money.add(new Money(currencyName,amount));
    }

    public synchronized void spendMoney(String currencyName, float amount){
        for (Money aMoney : this.money) {
            if (aMoney.getName().equals(currencyName)) {
                aMoney.subtract(amount);
                return;
            }
        }
    }
    synchronized public void begForMoney(){
        int fortune = new Random().nextInt(10);
        if (fortune > 7) {
            this.money.get(0).add(1000);
        }
    }
    private void buyCurrency(String currencyName, String givenCurrencyName, float givenAmount){
        this.panel.getCurrencyMarket().buyTransaction(this,givenCurrencyName,currencyName,givenAmount);
    }

    public String getPesel() {
        return pesel;
    }


    public void receiveMaterial(RawMaterial material) {
        if (rawMaterials.size()==0){
            rawMaterials.add(material);
            return;
        }
        for (RawMaterial entry : rawMaterials) {
            if (entry.getName().equals(material.getName())){
                entry.setAmount(entry.getAmount() + material.getAmount());
            }

        }
        rawMaterials.add(material);

    }

    public ObservableList<RawMaterial> getRawMaterials() {
        return rawMaterials;
    }

    public boolean discardMaterial(RawMaterial material, float amount) {
        for (RawMaterial entry : rawMaterials) {
            if (entry.getName().equals(material.getName())){
                if (entry.getAmount() > amount){
                    entry.setAmount(entry.getAmount() - amount);
                    return true;
                }
            }

        }
        return false;
    }

    public void lock() {
        this.locked = true;
    }
    public void unlock(){
        this.locked = false;
    }
}