package App.Investor;

import App.Money;
import App.RawMaterialsMarket.RawMaterial;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class InvestorController {
    @FXML private Label nameLabel;
    @FXML private Label peselLabel;
//    @FXML ListView sharesListView;
    @FXML private ListView<Money> moneyListView;
    @FXML private ListView<RawMaterial> rawMaterialsListView;

    private Investor model;
    public InvestorController(){

    }

    public void initModel(Investor investor){
        if (this.model != null){
            throw new IllegalStateException("Model can only be initialized once");
        }
        this.model = investor;
        nameLabel.setText(model.getName());
        peselLabel.setText(model.getPesel());
        ObservableList<Money> moneyObservable = this.model.getMoney();
        ObservableList<RawMaterial> rawMaterialObservable = this.model.getRawMaterials();
        moneyListView.setItems(moneyObservable);
        rawMaterialsListView.setItems(rawMaterialObservable);
    }
    private void refresh(){
        moneyListView.refresh();
        rawMaterialsListView.refresh();
        //
    }
    public void begForMoney(){
        this.model.begForMoney();
        this.refresh();
    }
}
