package App.Index;

import App.Company.Company;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Index {
    private String name;
    private ObservableList<Company> companies;
    public Index(){
        this.name = "regular index";
        companies = FXCollections.observableArrayList();
    }
    public Index(String name){
        this.name = name;
        companies = FXCollections.observableArrayList();
    }
    public void addCompany(Company co){
        companies.add(co);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String toString(){
        return this.name;
    }

    public ObservableList<Company> getCompanies() {
        return companies;
    }
    public void startWindow(){
        IndexWindow window = new IndexWindow(this);
    }
}
