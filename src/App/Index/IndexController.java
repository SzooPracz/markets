package App.Index;

import App.Company.Company;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

public class IndexController {
    @FXML private ListView<Company> companyListView;

    private Index model;
    public IndexController(){

    }
    public void initModel(Index index){
        if (this.model == null) {
            this.model = index;
        }
        ObservableList<Company> companyObservable = this.model.getCompanies();
        companyListView.setItems(companyObservable);
        companyListView.setMouseTransparent(true);
        companyListView.setFocusTraversable(false);
    }
}
