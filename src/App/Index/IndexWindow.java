package App.Index;

import App.MainWindow.MainWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class IndexWindow {
    public IndexWindow(Index model){
        Stage stage = new Stage();
        BorderPane root = new BorderPane();
        stage.initOwner(MainWindow.rootStage);
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader viewloader = new FXMLLoader(getClass().getResource("view.fxml"));
        try {
            root.setCenter(viewloader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        IndexController controller = viewloader.getController();
        controller.initModel(model);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
}
