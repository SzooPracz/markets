package App;

public class Asset {
    private String name;
    private float amount;

    public Asset(String name) {
        this.name = name;
    }
    public Asset(String name, float amount) {
        this.name = name;
        this.amount = amount;
    }
    public Asset(){

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }
}
