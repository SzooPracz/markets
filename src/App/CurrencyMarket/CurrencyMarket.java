package App.CurrencyMarket;

import App.Investor.Investor;
import App.Market;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Random;

public class CurrencyMarket extends Market {


    private ObservableList<CurrencyEntry> currencies;

    public CurrencyMarket(String name) {
        super(name);
        currencies = FXCollections.observableArrayList();
    }
    public void addCurrency(Currency currency){
        this.currencies.add(new CurrencyEntry(currency));
    }

    public ObservableList<CurrencyEntry> getCurrencies() {
        return this.currencies;
    }

    public void startWindow(Currency selectedCurrency) {
            CurrencyWindow window = new CurrencyWindow(this, selectedCurrency);
    }
    synchronized public void buyTransaction(Investor buyer, String currencyName, String wantedCurrencyName, float amountGiven){
        float currencyPoints = 0;
        float wantedCurrencyPoints = 0;

        for (CurrencyEntry currency : currencies) {
            if (currency.getCurrency().getName().equals(currencyName)) {
                currencyPoints = currency.getPoints();
            }
            if (currency.getCurrency().getName().equals(wantedCurrencyName)) {
                wantedCurrencyPoints = currency.getPoints();
            }
        }
        if ((currencyPoints != 0) && (wantedCurrencyPoints != 0)) {
            float wantedCurrencyPrice = wantedCurrencyPoints / currencyPoints;
            float amountReceive = amountGiven / wantedCurrencyPrice;
            amountReceive -= amountReceive * getMargin();
            buyer.spendMoney(currencyName, amountGiven);
            buyer.receiveMoney(wantedCurrencyName,amountReceive);
            System.out.println("transaction: ");
            System.out.println("Investor: " + buyer.getName() + ", given currency: " + currencyName + ", received currency: " +
                    wantedCurrencyName + ", amount spent: " + amountGiven + ", amount received: " + amountReceive);

        }
    }
    public void printEntries() {
        for (CurrencyEntry currency : currencies) {
            System.out.println(currency);
        }
    }

    public void update(int day) {
        for (CurrencyEntry entry : currencies) {
            float points = entry.getPoints();
            float change = new Random().nextInt(101) - 50;
            change = change / 1000;
            entry.setPoints(points + points*change);
        }
    }
}