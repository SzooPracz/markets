package App.CurrencyMarket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Currency {
    private static final List<String> allNames;
    public volatile static List<Currency> currenciesAlive;
    static{
        allNames = new ArrayList<>(
                Arrays.asList("dollar", "euro", "yen" , "krona", "peso", "won", "ruble", "franc", "yang")
        );
        currenciesAlive = new ArrayList<>();
    }

    private String name;
    private List<String> countries;


    public Currency(){
        int size = allNames.size();
        if (size==0){
            this.setName("Noname");
        } else {
            int picked = new Random().nextInt(size);
            this.setName(allNames.get(picked));
            allNames.remove(picked);
        }
        currenciesAlive.add(this);
        System.out.println("new currency created: " + name);


    }

    public String toString(){
        return name;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public boolean equals(Object other) {
        return other != null && (other == this || other instanceof Currency && this.name.equals(((Currency) other).getName()));
    }
}
