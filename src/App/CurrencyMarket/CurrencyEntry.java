package App.CurrencyMarket;

import java.util.Random;

public class CurrencyEntry {
    private Currency currency;
    private float points;
    public CurrencyEntry(Currency currency){
        this.currency = currency;
        this.points = new Random().nextInt(50) + 100;


    }
    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }
    public void addPoints(float points){
        this.points += points;
    }
    public void subtractPoints(float points){
        this.points -= points;
    }
    public String toString(){
        return this.currency.getName() + " : " + String.valueOf(this.points);
    }
}
