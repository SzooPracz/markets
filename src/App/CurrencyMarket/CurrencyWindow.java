package App.CurrencyMarket;

import App.MainWindow.MainWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class CurrencyWindow {

    public CurrencyWindow(CurrencyMarket market, Currency currency){

        Stage stage = new Stage();
        BorderPane root = new BorderPane();
        stage.initOwner(MainWindow.rootStage);
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader viewLoader = new FXMLLoader(getClass().getResource("view.fxml"));
        try {
            root.setCenter(viewLoader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
        CurrencyController controller = viewLoader.getController();
        controller.initModel(market, currency);

        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
}
