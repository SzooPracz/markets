package App.CurrencyMarket;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;


public class CurrencyController {
    @FXML private Label currencyNameLabel;
    @FXML private ListView<CurrencyEntry> entriesListView;
    @FXML private BarChart<String,Float> chart;
    private CurrencyMarket model;
    private Currency currency;
    private ObservableList<CurrencyEntry> entriesObservable;
    private float points;

    public CurrencyController(){

    }
    public void initModel(CurrencyMarket market, Currency currency){
        this.model = market;
        this.currency = currency;
        String currencyName = this.currency.getName();
        this.currencyNameLabel.setText(currencyName);
        int picked=-1;
        for (int i=0;i<model.getCurrencies().size();i++){
            if (model.getCurrencies().get(i).getCurrency().getName().equals(currencyName)){
                picked = i;
            }
        }
        points = this.model.getCurrencies().get(picked).getPoints();


        entriesObservable = this.model.getCurrencies();
        entriesListView.setItems(entriesObservable);
        // the price is calculated as (thisCurrencyPoints / points of another currency)
        entriesListView.setCellFactory(param -> new  ListCell<CurrencyEntry>() {
            @Override
            protected void updateItem(CurrencyEntry entry, boolean empty) {
                super.updateItem(entry, empty);

                if (empty || entry == null ){
                    setText(null);
                }else {
                    float thisCurrencyPoints = entry.getPoints();
                    setText(entry.getCurrency().getName() + ": " + String.valueOf(thisCurrencyPoints/points));
                }
            }
        });
        entriesListView.setFocusTraversable(false);
        initChart();

    }

    private void initChart() {
        XYChart.Series<String, Float> series = new XYChart.Series<>();
        series.setName("present prices of currencies in " + this.currency.getName());

        for (CurrencyEntry anEntriesObservable : entriesObservable) {
            String currencyName = anEntriesObservable.getCurrency().getName();
            float value = anEntriesObservable.getPoints() / points;
            if (!(currencyName.equals(this.currency.getName()))) {
                series.getData().add(new XYChart.Data<>(currencyName, value));
            }
        }
        this.chart.getData().add(series);

    }
}
