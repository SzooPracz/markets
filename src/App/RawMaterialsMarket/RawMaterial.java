package App.RawMaterialsMarket;

import App.Asset;



public class RawMaterial extends Asset {
    private String unit;
    public RawMaterial(){

    }
    public RawMaterial(String name, float amount, String unit){
        this.setName(name);
        this.setAmount(amount);
        this.setUnit(unit);

    }
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String toString(){
        return this.getName() + " " + this.getAmount() + " [" + getUnit() + "]";
    }
}
