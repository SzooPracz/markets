package App.RawMaterialsMarket;

import App.Investor.Investor;
import App.Market;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;

import java.sql.Array;
import java.util.*;


public class RawMaterialsMarket extends Market {
    public static List<String> rawMaterialsAlive;
    private static List<RawMaterialEntry> allEntries;
    static {
        allEntries = new ArrayList<>(
                Arrays.asList(
                        new RawMaterialEntry("Wood","kg", 100),
                        new RawMaterialEntry("Gold","ounce", 1000),
                        new RawMaterialEntry("Marble","kg",800),
                        new RawMaterialEntry("Antimatter","a little bit",800),
                        new RawMaterialEntry("Fleximetal","wagon",800)
                )
        );
        rawMaterialsAlive = new ArrayList<>();
    }
    private ObservableList<RawMaterialEntry> rawMaterialEntries;
    private HashMap<Integer,ArrayList<Pair<String,Float>>> history;

    public RawMaterialsMarket(String name) {
        super(name);
        setCurrency();
        history = new HashMap<>();
        rawMaterialEntries = FXCollections.observableArrayList();
    }
    public String toString(){
        return getName();
    }

    public void addRawMaterial() {

        int size = allEntries.size();
        if (size > 0) {
            int picked = new Random().nextInt(size);
            RawMaterialEntry chosen = allEntries.get(picked);
            rawMaterialEntries.add(chosen);
            System.out.println("raw material added: " + chosen.getName());
            rawMaterialsAlive.add(chosen.getName());
            allEntries.remove(picked);
        }
    }
    public void buyTransaction(Investor buyer, String materialName, float givenMoney){
        for (RawMaterialEntry entry : rawMaterialEntries){
            if (entry.getName().equals(materialName)){
                float price = entry.getPrice();
                float amountReceived = givenMoney/price;
                buyer.spendMoney(this.getCurrency().getName(),givenMoney);
                RawMaterial material = new RawMaterial(materialName,amountReceived,entry.getUnit());
                buyer.receiveMaterial(material);
                System.out.println("Raw Material buy : "+ buyer.getName() +", " + material.getName()
                        + " " + material.getAmount() + ", money spent:" + givenMoney + " "  + getCurrency().getName());
            }
        }
    }
    public ObservableList<RawMaterialEntry> getEntries() {
        return rawMaterialEntries;
    }

    synchronized public void sellTransaction(Investor seller, RawMaterial material, float amount) {
        float price;
        for (RawMaterialEntry rawMaterialEntry : rawMaterialEntries) {
            if (rawMaterialEntry.getName().equals(material.getName())){
                price = rawMaterialEntry.getPrice();
                float moneyBack = price * amount;
                moneyBack = moneyBack - moneyBack*this.getMargin();
                if (seller.discardMaterial(material,amount)) {
                    seller.receiveMoney(getCurrency().getName(),moneyBack);
                    System.out.println("Raw Material sell : "+ seller.getName() +", " + material.getName()
                            + " " + material.getAmount() + ", money received:" + moneyBack + " " + getCurrency().getName());
                }

                return;
            }
        }
    }

    synchronized public void update(int day) {
        ArrayList<Pair<String,Float>> today = new ArrayList<>();
        for (RawMaterialEntry entry : rawMaterialEntries){
            today.add(new Pair<>(entry.getName(),entry.getPrice()));
            float percent = new Random().nextInt(9) - 4;
            percent = percent / 100;
            entry.setPrice(entry.getPrice() + entry.getPrice()*percent);
        }
        history.put(day,today);
    }

    public HashMap<Integer, ArrayList<Pair<String, Float>>> getHistory() {
        return history;
    }

    public void startWindow(RawMaterialEntry selectedMaterial) {
        RawMaterialWindow window = new RawMaterialWindow(this, selectedMaterial);
    }
}
