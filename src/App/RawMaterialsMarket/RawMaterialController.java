package App.RawMaterialsMarket;


import App.Panel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.Axis;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.String.valueOf;


public class RawMaterialController {

    @FXML private LineChart<String,Float> chart;
    public void initModel(RawMaterialsMarket model, RawMaterialEntry selectedMaterial) {
        HashMap<Integer,ArrayList<Pair<String,Float>>> history = model.getHistory();

        XYChart.Series<String,Float> series = new XYChart.Series<>();
        series.setName(selectedMaterial.getName());

        for (Integer key : history.keySet()){
            System.out.println(key);
            ArrayList<Pair<String,Float>> oneDay = history.get(key);
            for (Pair<String,Float> pair : oneDay){

                if (pair.getKey().equals(selectedMaterial.getName())){
                    System.out.println(pair);
                    series.getData().add(new XYChart.Data<String, Float>(valueOf(key), pair.getValue()));
                }
            }

        }
        chart.getXAxis().setLabel("number of day");
        chart.getData().add(series);
    }
}
