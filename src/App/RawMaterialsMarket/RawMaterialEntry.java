package App.RawMaterialsMarket;

public class RawMaterialEntry {
    private String name;
    private String unit;
    private float price;


    public RawMaterialEntry(String name,String unit, float price){
        setName(name);
        setUnit(unit);
        setPrice(price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    public String toString(){
        return getName() + " : " + getPrice() + " / " + getUnit();
    }


}
